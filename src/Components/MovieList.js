import React from "react";
import PropTypes from "prop-types";
import MovieCard from "./MovieCard";
import "../ComponentStyle/MovieListStyle.css";

const getMovies = (movies) => {
  // console.log(movies);
  return (
    <div className="list">
      {movies.map((movie) => (
        <MovieCard key={movie.id} movie={movie} />
      ))}
    </div>
  );
};
const MovieList = (props) => <div>{getMovies(props.movies)}</div>;

MovieList.defaultProps = {
  movies: [],
};

MovieList.propTypes = {
  movies: PropTypes.array,
};

export default MovieList;
