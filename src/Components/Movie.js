import React, { Component } from "react";
import MovieData from "../MovieData/ MovieData";
import MovieList from "./MovieList";

class Movies extends Component {
  constructor() {
    super();

    this.state = {
      movies: [],
    };
  }

  componentDidMount() {
    this.setState(() => ({ movies: MovieData.getMovies() }));
  }
  render() {
    return (
      <div>
        <MovieList movies={this.state.movies} />
      </div>
    );
  }
}

export default Movies;
