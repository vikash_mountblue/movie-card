import React, { Component } from "react";
import "../ComponentStyle/HeaderStyle.css";
class Header extends Component {
  render() {
    return (
      <div>
        <nav>
          <div className="header">
            <h2>Movie Cards</h2>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;
