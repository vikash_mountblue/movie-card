import React from "react";
import PropTypes from "prop-types";
import "../ComponentStyle/MovieCardStyle.css";

const MovieCard = (props) => (
  <div className="movie-card card">
    <div>
      <img className="image-size" src={props.movie.imageUrl} alt="images" />
    </div>
    <div className="movie-details">
      <h3 className="movie-title">{props.movie.title}</h3>
      <p>{props.movie.year}</p>
      <p className="movie-description">{props.movie.description}</p>
    </div>
    <div className="card-footer">
      <hr></hr>
      <div className="card-footer-badge ">
        <p>Rating: {props.movie.rating}/5</p>
      </div>
    </div>
  </div>
);

MovieCard.defaultProps = {
  movie: {},
};

MovieCard.propTypes = {
  movie: PropTypes.object,
};

export default MovieCard;
