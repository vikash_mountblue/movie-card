import movies from "./movies.json";

export default class MovieData {
  static getMovies() {
    return movies ? movies : [];
  }
}
