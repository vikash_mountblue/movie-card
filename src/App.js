import React from "react";
import Header from "./Components/Header";
import Movies from "./Components/Movie";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Header />
      <div>
        <Movies />
      </div>
    </div>
  );
}

export default App;
